# The first instruction is what image we want to base our container on
# We Use an official Python runtime as a parent image
FROM python:3.6

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /testing_celery_rabbit_incontainer 
# RUN echo "can i see this"

# Set the working directory to /testing_celery_rabbit_incontainer
WORKDIR /testing_celery_rabbit_incontainer

# Copy the current directory contents into the container at /testing_celery_rabbit_incontainer
ADD . /testing_celery_rabbit_incontainer/

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Server
# EXPOSE 8000
# STOPSIGNAL SIGINT
# ENTRYPOINT ["python", "manage.py"]
# CMD ["runserver", "0.0.0.0:8000"]