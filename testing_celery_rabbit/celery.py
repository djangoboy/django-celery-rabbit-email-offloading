import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testing_celery_rabbit.settings')

app = Celery('testing_celery_rabbit')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()